import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./modules/general/home/home.component";
import { NotFoundComponent } from "./modules/general/not-found/not-found.component";
import { AppRoutingModule } from "./app-routing.module";
import { MailingModule } from "./modules/general/contact/mailing/mailing.module";
import { MailingComponent } from "./modules/general/contact/mailing/mailing.component";
import { MapModule } from "./modules/general/contact/map/map.module";
import { MapComponent } from "./modules/general/contact/map/map.component";
import { TestComponent } from "./shared/test/test.component";
import { ComponentsModule } from "./modules/application/components/components.module";
import { ServicesComponent } from "./modules/application/services/services.component";
import { ServicesModule } from "./modules/application/services/services.module";
import { TemplateDrivenFormsModule } from "./modules/application/template-driven-forms/template-driven-forms.module";
import { TemplateDrivenFormsComponent } from "./modules/application/template-driven-forms/template-driven-forms.component";
import { FormsModule } from "@angular/forms";
import { ItemsComponent } from "./modules/application/items/items.component";
import { ItemsModule } from "./modules/application/items/items.module";
import { HttpClientModule } from "@angular/common/http";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    MailingComponent,
    MapComponent,
    TestComponent,
    ServicesComponent,
    TemplateDrivenFormsComponent,
    ItemsComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    AppRoutingModule,
    MailingModule,
    MapModule,
    FormsModule,
    ComponentsModule,
    ServicesModule,
    HttpClientModule,
    BrowserModule,
    TemplateDrivenFormsModule,
    ItemsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
