import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ServicesRoutingModule } from './services-routing.module';
import { SafePipe } from './safe.pipe';

@NgModule({
  imports: [
    CommonModule,
    ServicesRoutingModule
  ],
  exports: [
    SafePipe
  ],
  declarations: [
    SafePipe
  ],
  providers: [
  ],
})
export class ServicesModule { }