import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsRoutingModule } from './items-routing.module';
import { ItemsService } from './items.service';

@NgModule({
  imports: [
    CommonModule,
    ItemsRoutingModule,
  ],
  exports: [
   
  ],
  declarations: [
   
  ],
  providers: [ItemsService],
})
export class ItemsModule { }