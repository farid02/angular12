import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChartsModule } from "ng2-charts";
import { ChartjsRoutingModule } from "./chartjs-routing.module";

@NgModule({
  declarations: [],
  exports: [],
  imports: [CommonModule, ChartsModule, ChartjsRoutingModule],
})
export class ChartjsModule {}
